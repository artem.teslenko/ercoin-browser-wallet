(ns ^:figwheel-no-load ercoin-wallet.dev
  (:require
    [ercoin-wallet.core :as core]
    [devtools.core :as devtools]))


(enable-console-print!)

(devtools/install!)

(core/init!)
