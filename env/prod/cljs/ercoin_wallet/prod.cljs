(ns ercoin-wallet.prod
  (:require
    [ercoin-wallet.core :as core]))

;;ignore println statements in prod
(set! *print-fn* (fn [& _]))

(core/init!)
