(defproject ercoin-wallet "0.1.0-SNAPSHOT"
  :description "An in-browser Ercoin wallet"
  :url "https://gitlab.com/Ercoin/ercoin-browser-wallet"
  :license {:name "Apache License 2.0"
            :url "https://www.apache.org/licenses/LICENSE-2.0.html"}

  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/clojurescript "1.10.439"]
                 [alandipert/storage-atom "2.0.1"]
                 [antizer "0.3.1"]
                 [com.taoensso/tempura "1.2.1"]
                 [io.replikativ/hasch "0.3.6"]
                 [reagent "0.8.1"]
                 [cljs-http "0.1.45"]
                 [mvxcvi/alphabase "1.0.0"]
                 [cljsjs/nacl-fast "1.0.0-rc.1-0"]]

  :plugins [[lein-cljsbuild "1.1.7"]
            [lein-doo "0.1.11"]
            [lein-figwheel "0.5.17"]
            [lein-kibit "0.1.6"]]

  :doo {:paths {:karma "./node_modules/karma/bin/karma"}}

  :min-lein-version "2.5.0"

  :clean-targets ^{:protect false}
  [:target-path
   [:cljsbuild :builds :app :compiler :output-dir]
   [:cljsbuild :builds :app :compiler :output-to]]

  :resource-paths ["public"]

  :figwheel {:http-server-root "."
             :nrepl-port 7002
             :nrepl-middleware ["cemerick.piggieback/wrap-cljs-repl"]
             :css-dirs ["public/css"]}

  :cljsbuild {:builds {:app
                       {:source-paths ["src" "env/dev/cljs"]
                        :compiler
                        {:main "ercoin-wallet.dev"
                         :output-to "public/js/app.js"
                         :output-dir "public/js/out"
                         :asset-path   "js/out"
                         :source-map true
                         :optimizations :none
                         :pretty-print  true}
                        :figwheel
                        {:on-jsload "ercoin-wallet.core/mount-root"
                         :open-urls ["http://localhost:3449/index.html"]}}
                       :release
                       {:source-paths ["src" "env/prod/cljs"]
                        :compiler
                        {:main "ercoin-wallet.prod"
                         :output-to "public/js/app.js"
                         :output-dir "public/js/release"
                         :asset-path   "js/release"
                         :optimizations :advanced
                         :pretty-print false}}
                       :test
                       {:source-paths ["src" "test"]
                        :compiler {:output-to "public/js/test.js"
                                   :main ercoin-wallet.runner
                                   :optimizations :none}}}}

  :aliases {"package" ["do" "clean" ["cljsbuild" "once" "release"]]}

  :profiles {:dev {:dependencies [[org.clojure/test.check "0.9.0"]
                                  [binaryage/devtools "0.9.10"]
                                  [figwheel-sidecar "0.5.17"]]}})
