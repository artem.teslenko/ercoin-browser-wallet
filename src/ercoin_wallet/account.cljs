(ns ercoin-wallet.account
  (:require
   [clojure.spec.alpha :as s]
   [ercoin-wallet.uint :as uint]
   ))

(s/def ::account (s/keys :req [::address]
                         :opt [::private-key ::label ::valid-until ::locked-until ::validator-address ::balance]))

(defn nacl-keypair->keypair [nacl-keypair]
  (let [nacl-privkey (aget nacl-keypair "secretKey")
        nacl-pubkey (aget nacl-keypair "publicKey")]
    {::address (js->clj (js/Array.from nacl-pubkey))
     ::private-key (js->clj (js/Array.from nacl-privkey))}))

(defn new-keypair []
  (nacl-keypair->keypair (js/nacl.sign.keyPair)))

(defn private-key->keypair [private-key]
  (-> private-key
      uint/vec->uint8arr
      js/nacl.sign.keyPair.fromSecretKey
      nacl-keypair->keypair))

(defn add-keypair [{:keys [::address ::private-key] :as account}]
  (if (empty? address)
    (let [keypair (if private-key
                    (private-key->keypair private-key)
                    (new-keypair))]
      (merge account keypair))
    account))

(defn deserialize [address account-bin]
  {::address address
   ::valid-until (when (seq account-bin)
                   (uint/vec->uint (subvec account-bin 0 4)))
   ::locked-until (when (>= (count account-bin) 16)
                    (uint/vec->uint (subvec account-bin 12 16)))
   ::validator-address (when (>= (count account-bin) 16)
                         (subvec account-bin 16 48))
   ::balance (when (seq account-bin)
               (uint/vec->uint (subvec account-bin 4 12)))})
