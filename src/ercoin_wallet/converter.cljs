(ns ercoin-wallet.converter)

(defprotocol Converter
  (encode [this from])
  (decode [this from]))
