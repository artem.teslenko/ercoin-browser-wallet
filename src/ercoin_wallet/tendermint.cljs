(ns ercoin-wallet.tendermint
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require
   [alphabase.hex :as hex]
   [antizer.reagent :as ant]
   [cljs.core.async :refer [<! promise-chan]]
   [cljs-http.client :as http]
   [clojure.string :refer [split]]
   [ercoin-wallet.account :as a]
   [ercoin-wallet.binary :as binary]
   [ercoin-wallet.converter :as converter]
   [ercoin-wallet.tx :as tx]
   [goog.crypt.base64 :as b64]
   [goog.string :as gstring]
   [goog.string.format]
   ))

(enable-console-print!)

(defn request
  ([endpoint method]
   (request endpoint method {}))

  ([endpoint method params]
   (go
     (let [url (str endpoint "/" method)
           resp-chan (promise-chan)
           response (<! (http/get url
                                  {:query-params params
                                   :with-credentials? false}))]
       (println response)
       (if (and
            (= 200 (:status response))
            (:success response))
         (get-in response [:body :result])
         (do (ant/message-error "Error communicating with server." 5)
             false))))))

(defn query [endpoint path data]
  (go
    (let [params {:path (str "\"" path "\"")
                  :data (str "0x" (hex/encode (clj->js data)))}
          result (<! (request endpoint "abci_query" params))
          value (get-in result [:response :value] "")]
      (assert value)
      (js->clj (b64/decodeStringToByteArray value))
      )))

(defn height [endpoint]
  (go
    (let [resp-chan (promise-chan)
          result (<! (request endpoint "abci_info"))]
      (js/parseInt (get-in result [:response :last_block_height])))))

(defn tx-search [endpoint query page]
  (go
    (->> query
         (filter #(or (not (seqable? (second %)))
                      (seq (second %))))
         (map #(gstring/format (if (string? (second %))
                                 "%s='%s'"
                                 "%s=%s")
                               (first %)
                               (second %)))
         (clojure.string/join " AND ")
         (gstring/format "\"%s\"")
         (assoc {:page page} :query)
         (request endpoint "tx_search")
         <!
         :txs
         (filter #(zero? (get-in % [:tx_result :code] 0)))
         (map tx/from-rpc-info)
         (sort-by #(::tx/height (meta %))))))

(defn error-description [code]
  (case code
    0 "OK"
    1 "bad request"
    2 "not found"
    3 "forbidden"
    4 "insufficient funds"
    5 "invalid timestamp"
    6 "already executed"))
