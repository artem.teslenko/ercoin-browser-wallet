(ns ercoin-wallet.terms
  (:require
   [antizer.reagent :as ant]
   [ercoin-wallet.i18n :refer [tr]]
   [hasch.core :as hasch]))

(def terms
  (tr [:en] :terms/terms nil))

(def terms-localized
  (tr :terms/terms))

(defn checksum [data]
  (hasch/edn-hash data))

(def terms-checksum
  (checksum terms))
