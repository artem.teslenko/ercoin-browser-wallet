(ns ercoin-wallet.uint
  "Conversions between byte vectors, strings, integers"
  (:require
   [clojure.spec.alpha :as s]
   [goog.crypt :as crypt]
   ))

(defonce utf8-decoder (new js/TextDecoder "utf-8"))
(defonce utf8-encoder (new js/TextEncoder "utf-8"))

(defn uint8arr->vec [arr]
  (js->clj (js/Array.from arr)))

(defn vec->uint8arr [binary]
  (-> binary
      js->clj
      js/Uint8Array.from))

(defn str->utf8 [string]
  (-> string
      (goog.crypt.stringToUtf8ByteArray)
      (js->clj)))

(s/fdef str->utf8
        :args (s/cat :string string?)
        :ret (s/coll-of int? :kind vector?))

(defn utf8->str [bin]
  (-> bin
      clj->js
      goog.crypt.utf8ByteArrayToString))

(s/fdef utf8->str
        :args (s/cat :utf8 (s/coll-of int? :kind vector?))
        :ret string?)

(defn vec->uint [uint-bin]
  (loop [acc 0
         rem-uint-bin uint-bin]
    (if (zero? (count rem-uint-bin))
      acc
      (recur (+ (* acc 256) (nth rem-uint-bin 0))
             (subvec rem-uint-bin 1)))))

(s/fdef vec->uint
        :args (s/coll-of int? :kind vector?)
        :ret int?)

(defn uint->vec [uinteger length]
  (vec
   (rseq
    (loop [acc []
           rem-length length
           rem-uint uinteger]
      (if (zero? rem-length)
        acc
        (let [digit (rem rem-uint 256)]
          (recur (conj acc digit)
                 (dec rem-length)
                 (quot (- rem-uint digit) 256))))))))

(s/fdef uint->vec
        :args (s/cat :uinteger int?
                     :length int?)
        :ret (s/coll-of int? :kind vector?))
