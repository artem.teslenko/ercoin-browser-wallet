(ns ercoin-wallet.units
  (:require
   [clojure.string :as string]
   [ercoin-wallet.converter :refer [Converter]]
   [ercoin-wallet.i18n :refer [tr]]
   ))

(defrecord Unit [name key symbol resolution]
  Converter
  (encode [this value]
    (let [divider (js/Math.pow 10 resolution)
          integer-part (quot value divider)
          fractional-part (rem value divider)]
      (if (pos? fractional-part)
        (let [fractional-str-raw (str fractional-part)]
          (str
           integer-part
           "."
           (clojure.string/join (repeat (- resolution (count fractional-str-raw)) 0))
           (string/replace fractional-str-raw #"0+$" "")))
        (str integer-part))))
  (decode [this value-str]
    (let [[integer-str fractional-str] (string/split value-str ".")
          integer-part (* (js/parseInt integer-str) (js/Math.pow 10 resolution))]
      (if (and fractional-str (pos? resolution))
        ;; If we need to truncate fractional part, then technically it is invalid amount (precision too high), but we treat it gracefully due to form inputs.
        (let [fractional-str-truncated (subs fractional-str 0 resolution)]
          (+ integer-part
             (* (js/parseInt fractional-str-truncated)
                (js/Math.pow 10 (- resolution (count fractional-str-truncated))))))
        integer-part))))

(def ercoin (Unit. (tr :units/ercoin) :ercoin "ERN" 6))
(def deciercoin (Unit. (tr :units/deciercoin) :deciercoin "dERN" 5))
(def centiercoin (Unit. (tr :units/centiercoin) :centiercoin "cERN" 4))
(def miliercoin (Unit. (tr :units/miliercoin) :miliercoin "mERN" 3))
(def microercoin (Unit. (tr :units/microercoin) :microercoin "µERN" 0))
(def kiloercoin (Unit. (tr :units/kiloercoin) :kiloercoin "kERN" 9))
(def megaercoin (Unit. (tr :units/megaercoin) :megaercoin "MERN" 12))

(def all (list
          ercoin
          deciercoin
          centiercoin
          miliercoin
          microercoin
          kiloercoin
          megaercoin
         ))
(def all-map
  (into {} (map (fn [unit] [(:key unit) unit]) all)))
(def all-map-symbols
  (into {} (map (fn [unit] [(:symbol unit) unit]) all)))

(defn get-unit [key]
  (get all-map key))

(defn get-unit-by-symbol [symb]
  (get all-map-symbols symb))
