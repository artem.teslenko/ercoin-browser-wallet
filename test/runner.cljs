(ns ercoin-wallet.runner
  (:require [doo.runner :refer-macros [doo-tests]]
            [ercoin-wallet.tx-test]))

(doo-tests 'ercoin-wallet.tx-test)
